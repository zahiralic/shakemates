terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.37.0"
    }
  }
}

provider "aws" {
  # Configuration options
  profile = "academy"
  region  = "us-east-1"
}

# Setting up Hosted Zone for gitlab
# resource "aws_route53_zone" "gitlab_zone" {
#   name = "gitlab.shakemat.es"

#   tags = {
#     Environment = "Prod"
#   }
# }

resource "aws_route53_record" "www" {
  zone_id = "Z063892335XFR580UF6A1"
  name    = "gitlab.shakemat.es"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.gitlab_instance.public_ip]
}

resource "aws_route53_record" "wiki" {
  zone_id = "Z063892335XFR580UF6A1"
  name    = "wiki.shakemat.es"
  type    = "A"
  ttl     = "300"
  records = [aws_instance.wikijs_instance.public_ip]
}

data "aws_ami" "ubuntu_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "gitlab_instance" {
  ami           = data.aws_ami.ubuntu_ami.id
  instance_type = "t3.medium"
  key_name      = "shakemates"

  tags = {
    Name = "Gitlab Instance"
  }
}

# WikiJS

locals {
  vpc_id = "vpc-fe49e483"
}

resource "aws_security_group" "wikijs" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = local.vpc_id

  ingress {
    description      = "TLS from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  ingress {
    description      = "TLS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_tls"
  }
}

resource "aws_instance" "wikijs_instance" {
  ami             = data.aws_ami.ubuntu_ami.id
  instance_type   = "t3.small"
  key_name        = "shakemates"
  user_data       = file("install_docker.sh")
  security_groups = [aws_security_group.wikijs.name]
  tags = {
    Name = "WikiJS Instance"
  }
}